#!/usr/bin/python
# -*- coding: utf-8 -*-
 
import sys
from lxml import etree

sys.path.append('pyaccounting')
from GrisbiDocument import *

if len(sys.argv) != 2:
    print "Usage: fix-recur-transfers.py <file>"
    sys.exit ( 1 )

try:
    doc = GrisbiDocument ( sys.argv [ 1 ] )
except Exception as e:
    print "Error:", e
    sys.exit ( 1 )

for transaction in doc.find_transactions ( { 'account': doc.find_account ( u'Crédit Mutuel' ) } ):
    if transaction.get_financial_year ( ) == '-2':
        transaction.set_financial_year_by_date ( transaction . get_date ( ) )

doc.serialize ( 'out.gsb' )

