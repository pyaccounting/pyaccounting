#!/usr/bin/python
# -*- coding: utf-8 -*-
 
import sys
from lxml import etree

sys.path.append('../pydtc')
from pyDTC import *

from sqlalchemy import or_

dtc=pyDTC('mysql://root:toto55@localhost/dtc', False)
for e in dtc.query(Subscription).\
        filter ( Subscription.payment_mode == 'prelevement' ).\
        filter ( Subscription.prelevement_start_date != None ).\
        filter ( Subscription.prelevement_start_date != 0 ).\
        filter ( or_ \
                 ( Subscription.prelevement_start_date < Subscription.start_date, \
                   Subscription.prelevement_start_date > Subscription.end_date ) ):
    if e.membership.actor.actor_type == 'person':
        try:
            name = e.membership.actor.person.fullname()
        except:
            name = e.membership.actor.entity.fullname()

        if e.prelevement_start_date and e.prelevement_start_date > e.end_date:
            print "WRONG START DATE FOR %s" % name
            for prev in dtc.query(Subscription).\
                filter ( Subscription.payment_mode == 'prelevement' ).\
                filter ( Subscription.prelevement_start_date != None ).\
                filter ( Subscription.membership_id == e.membership.membership_id ).\
                filter ( Subscription.start_date < e.start_date ).\
                order_by ( Subscription.subscription_id.desc ( ) ).\
                limit ( 1 ):
                d = prev.prelevement_start_date
                while d <= e.start_date:
                    d = e.increment_prelevement_date ( d )
                    print "Incrementing to %s" % d
                e.prelevement_start_date = d

sys.exit(0)
