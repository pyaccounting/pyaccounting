#!/usr/bin/python
# -*- coding: utf-8 -*-

import sys
from lxml import etree
import string
import re

sys.path.append ( 'pyaccounting' )
from GrisbiDocument import *

if len ( sys.argv ) != 2:
    print ( 'usage: clean-payees.py <grisbi_file>' )
    exit ( 1 )

try:
    doc = GrisbiDocument ( sys.argv [ 1 ] )
except:
    print ( 'Unable to load "%s"' % sys.argv [ 1 ] )
    exit ( 1 )

doc.serialize ( 'before.gsb' )

print '============================================================'
print "> Fixing case"
payees = doc.get_payees ( )
for p in payees:
    p . set_name ( p . ucwords ( p.get_name ( ) ) )

print '============================================================'
print "> Removing TPE payees"
tpe = doc.find_payee ( 'TPE' )
payees = doc.get_payees ( )
for p in payees:
    name = p.get_name ( )
    if name is not None and name [ 0 : 3 ] == 'TPE' and \
           name  != 'TPE':
        doc.merge_payees ( tpe, p )
    

print '============================================================'
print "> Merging payees with similar names"

signs = re.compile (r'[-_ "\. \\/\\\'!?(),]+')
hash = {}
payees = doc.get_payees ( )
for p in payees:
    if p . get_name ( ):
	name = p.get_unac_name ( )
        name = name.lower ( )
        name = signs.sub ( ' ', name )
        if name == '' or name == ' ':
            name= '?'

	if hash . has_key ( name ):
	    hash [ name ] . append ( p . get_id ( ) )
	else:
	    hash [ name ] = [ p . get_id ( ) ]

for h, v in hash.iteritems ( ):
    if len(v) > 1:
        print len(v), "payees for", h, ":", \
          ', '.join (map ((lambda x: doc.find_payee_by_id (x).get_name ()), v))
	ref = doc.find_payee_by_id ( v . pop ( 0 ) )
	for p in v:
	    payee = doc.find_payee_by_id ( p )
	    if payee:
		doc.merge_payees ( ref, payee )
doc.serialize ( 'after.gsb' )

print '============================================================'

