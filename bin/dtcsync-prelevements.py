#!/usr/bin/python
# -*- coding: utf-8 -*-
 
import sys
from lxml import etree
from decimal import *
from datetime import * 

sys.path.append('pyaccounting')
from GrisbiDocument import *

sys.path.append('../pydtc')
from pyDTC import *

if len(sys.argv) != 2:
    print "Usage: dtcsync-prelevement.py <file>"
    sys.exit ( 1 )

try:
    doc = GrisbiDocument ( sys.argv [ 1 ] )
except Exception as e:
    print "Error:", e
    sys.exit ( 1 )

dtc=pyDTC('mysql://dtc:toto55@localhost/dtc', False)

prelevement_payee = doc.find_payee ( 'Prelevement par CB', True )
prelevement_cion_payee = doc.find_payee ( 'Cion Prelevement par CB', True )
prelevement_account = doc.find_account ( 'TPE récurrent' )
if not prelevement_account:
    raise Exception ( 'Unable to find account.' );

( prelevement_categ, prelevement_subcateg ) = doc.find_category_sub_category ( '7560' )


global_cions = {}
global_sums = {}
global_counts = {}


def register(e, amount, d):
    q = dtc.query(Non_Payment).\
            filter ( Non_Payment.date > func.ADDDATE ( d, - 3 ) ).\
            filter ( Non_Payment.date < func.ADDDATE ( d, 3 ) ).\
            filter ( Non_Payment.amount == amount ).\
            filter ( Non_Payment.voucher == e.voucher )
    if q.count():
        print "[31;1mNon payment[m for %s: %s, %.2f" % ( d, e.voucher, amount )
        return None

    cion = Decimal ( round ( Decimal(amount) * Decimal(Decimal(-e.commission_percentage)/Decimal(100))-Decimal(e.commission_fixed), 2 ) )
    if ( cion > -e.commission_minimum ): cion = -e.commission_minimum

    if not str(d) in global_cions:
        global_cions [ str(d) ] = Decimal ( 0 )
    global_cions [ str(d) ] += Decimal ( cion )
#    print "%s: %.2f, adding cion %.2f" % ( str(d), amount, cion )

    if not str(d) in global_sums:
        global_sums [ str(d) ] = Decimal ( 0 )
    global_sums [ str(d) ] += Decimal ( amount )
    return 1
    
    

### Get commissions
print 'Compute proratas...'
#        filter ( Subscription.start_date <= func.now ( ) ).\
for e in dtc.query(Subscription).\
        filter ( Subscription.start_date <= func.now ( ) ).\
        filter ( Subscription.payment_received_date != None ).\
        filter ( Subscription.payment_mode == 'cbweb_recur' ).\
        order_by ( func.date(Subscription.payment_received_date) ):

    if e.cbrecur_prorata == None or  e.cbrecur_prorata == 0:
        continue

    if e.membership.actor.actor_type == 'person':
        try:
            name = e.membership.actor.person.fullname()
        except:
            print "FAIL", e.membership.actor
    else:
        name = e.membership.actor.entity.fullname()
    payee = doc.find_payee ( name, True )
    if not payee:
        payee = doc.create_payee ( name )

    if register ( e, e.cbrecur_prorata, datetime.date(e.payment_received_date) ):
        t = prelevement_account . create_transaction ( payee )
        t.set_category ( prelevement_categ, prelevement_subcateg )
        t.set_amount ( e.cbrecur_prorata )
        t.set_comment ( u"Adhésion personne physique saisie auto (%s)" % e.voucher )
        t.set_reference ( e.voucher )
        t.set_voucher ( e.voucher )
        t.set_financial_year ( e.payment_received_date )
        t.set_date ( e.payment_received_date )
#        print 'ADDING CCION', t.get_date(), t, e.voucher


print 'Get payments...'
# Get payments 
for e in dtc.query(Subscription).\
        filter ( Subscription.start_date <= func.now ( ) ).\
        filter ( Subscription.payment_received_date != False ).\
        filter ( Subscription.payment_mode == 'cbweb_recur' ):

    if e.membership.actor.actor_type == 'person':
        try:
            name = e.membership.actor.person.fullname()
        except:
            print e.membership.actor
    else:
        name = e.membership.actor.entity.fullname()

    payee = doc.find_payee ( name, True )
    if not payee:
        print "[32;1m✓ Created payee %s[m" % name
        payee = doc.create_payee ( name )
    else:
        print "[32;1m✓ Found payee %s[m" % name

    found = 0
    transactions = payee.get_transactions ( )

    if not e.prelevement_start_date:
        # XXX CHECK THE HINT (I.e. for dodji it is not working correctly)
        hint = None
        if transactions:
            hint = transactions [ 0 ] . get_date ( );
            year = e.start_date.year
            month = e.start_date.month
            day = hint.day
            if hint.day > e.start_date.day:
                month = e.start_date.month + 1
                if month > 12:
                    month = 1
                    year += 1
            e.prelevement_start_date = date ( year, month, day )            
            print "[34;1m⚠ No start date for %s, hint is %s (%s → %s)[m" % ( name, e.prelevement_start_date, e.start_date, e.end_date )
        else:
            print "[31;1m⚠ No start date for %s and no hint[m" % name

    if not e.prelevement_periodicity:
        print "[31;1m⚠ No periodicity date for %s[m" % name
    elif e.prelevement_start_date and e.prelevement_start_date > e.end_date:
        print "[31;1m⚠ Wrong beginning for %s (%s > %s → %s)[m" % ( name, e.prelevement_start_date, e.start_date, e.end_date )
    elif e.prelevement_start_date:
        end = e.prelevement_end_date
        if not end or end > e.end_date:
            end = e.end_date

        d = date ( e.prelevement_start_date.year, 
                   e.prelevement_start_date.month,
                   e.prelevement_start_date.day )
#        print " ", e
        while d < e.start_date and d < date.today ( ):
            d = e.increment_prelevement_date ( d, 5 )
#        print " ", d
            
        matched = 0
        total = 0
        while ( d <= end and d <= date.today ( ) ):
            total += 1
            found = 0
            if transactions:
                for transaction in transactions:
                    account = transaction.get_account ( )
                    note = transaction . get_comment ( ) 
                    if account . get_name ( ) == 'TPE récurrent' and \
                            transaction . get_date () . month == d . month and \
                            transaction . get_date () . year == d . year and \
                            transaction . get_date () . day == d . day and \
                            transaction.get_reference() == e.voucher:
                        found += 1
                        matched += 1
#                        print "  Found payment for date %s: %s" % ( transaction . get_date ( ), transaction )

            if register ( e, e.prelevement_amount, d ):
                if not found:
                    print "  Found no prelevement transaction for %s and date %s, creating %.2f" % ( name, e.voucher, e.prelevement_amount )
                    if not prelevement_account:
                        raise Exception ( 'Unable to find account.' );
                    t = prelevement_account . create_transaction ( payee )
                    t.set_category ( prelevement_categ, prelevement_subcateg )
                    t.set_amount ( e.prelevement_amount )
                    t.set_comment ( u"Adhésion personne physique saisie auto (%s)" % e.voucher )
                    t.set_reference ( e.voucher )
                    t.set_voucher ( e.voucher )
                    t.set_financial_year ( d )
                    t.set_date ( d )
                    print 'ADDING', t.get_date(), e.voucher, e.prelevement_amount

            d = e.increment_prelevement_date ( d, 5 )

        if total == matched:
            print "  All matched %d/%s" % ( matched, total )

sums_left = Decimal ( 0 )
cions_left = Decimal ( 0 )
keys = global_cions.keys ( )
keys.sort()
for s in keys:
    good = 0
    while not good:
        print ">>> ", s
        matched = 0
        if not s in global_cions:
            global_cions[s] = 0
            global_sums[s] = 0
        global_cions[s] = Decimal ( round ( global_cions[s] + cions_left, 2 ) )
        global_sums[s] = Decimal ( round ( global_sums[s] + sums_left, 2 ) )
        transactions = prelevement_payee.get_transactions ( { 'date': s } )
        cions = prelevement_cion_payee.get_transactions ( { 'date': s } )
        cions_left = sums_left = Decimal ( 0 )
        for t in transactions:
            if t.get_amount() != global_sums[s]:
                if t.get_amount() != global_sums[s]:
                    print "[31;1mIncoherence[m for %s: %.2f != %.2f" % ( s,  t.get_amount(), global_sums[s])
            matched = 1
        for t in cions:
            if round(t.get_amount(),2) - round ( global_cions[s], 2 ) > 0.01:
                print "[31;1mIncoherence cion[m for %s: %.2f != %.2f" % ( s, round(t.get_amount(),2), round ( global_cions[s], 2 ) )
            matched = 1
    
        if not matched:
            print "Nothing for %s (%.2f, %.2f)" % ( s,  global_sums[s], global_cions[s] )
            tomorrow = (datetime.strptime(s, '%Y-%m-%d') + timedelta(1))
            sums_left += Decimal ( global_sums[s] )
            cions_left += Decimal ( global_cions[s] )
            if tomorrow:
                s = tomorrow.strftime('%Y-%m-%d')
        else:
            print "OK for %s (%.2f, %.2f)" % ( s,  global_sums[s], global_cions[s])
            good = 1
    
doc.serialize ( 'out.gsb' )

sys.exit(0)
