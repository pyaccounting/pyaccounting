#!/bin/bash

dtcsync_path=$(dirname $0)
treso_path=/var/www/adherents.april.org/tresorerie
treso_file=${treso_path}/tresorerie.gsb
log_file=${dtcsync_path}/logs/$1

export LANG=fr_FR.UTF-8
date > ${log_file}
echo "<h2>Updating subversion</h2>" >> ${log_file}
echo "<pre>" >> ${log_file}
cd ${treso_path}
svn update ${treso_file} >> ${log_file} 2>&1
echo "</pre>" >> ${log_file}
if test $? -ne 0 ; then
    echo "<strong>SVN error</strong>" >> ${log_file}
    echo "<h2>*** FIN ***</h2>" >> ${log_file}
    exit 1
fi

cd ${dtcsync_path}
python ${dtcsync_path}/dtcsync.py ${treso_file} >> ${log_file} 2>&1

echo "<h2>Committing subversion</h2>" >> ${log_file}
echo "<pre>" >> ${log_file}
cd ${treso_path}
svn commit -m "Saisie auto $(date +%F) (pyaccounting)" ${treso_file} >> ${log_file} 2>&1
echo "</pre>" >> ${log_file}
if test $? -ne 0 ; then
    echo "<strong>SVN error</strong>" >> ${log_file}
    exit 1
fi


echo "<h2>*** FIN ***</h2>" >> ${log_file}

