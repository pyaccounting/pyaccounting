# -*- coding: utf-8 -*-

import sys, re

sys.path.append('../../pydtc/pyDTC')
from pyDTC import *

sys.path.append('../pyaccounting')
from GrisbiDocument import *

from registration import register

def import_cbweb ( doc ):
    dtc=pyDTC('mysql://','pydtc.conf')
    cm = doc.find_account ( u'Crédit Mutuel' )
    tpe = doc.find_account ( u'TPE immédiat' )
    if not tpe:
        print "Unable to find account"

    start_date = '-'.join ( re.match ( 'Sync auto (\d\d)/(\d\d)/(\d\d\d\d)', cm.get_comment ( ) ).group(3,2,1) )
    print """<h2>Importing DTC from %s</h2>\n
    <table class="supertable">
    <tr><th>Day</th><th>Category</th><th>Date</th><th>Amount</th></tr>
    """ % start_date

    for e in dtc.query(Subscription).\
            filter ( Subscription.payment_received_date != None ).\
            filter ( Subscription.payment_received_date >= start_date ).\
            filter ( Subscription.payment_received_date < func.date(func.now()) ).\
            filter ( Subscription.payment_mode == 'cbweb' ):
        if e.membership.actor.actor_type == 'person':
            name = e.membership.actor.person.fullname()
            ( c, s ) = doc.find_category_sub_category ( '7560' )
        else:
            name = e.membership.actor.entity.fullname()
            ( c, s ) = doc.find_category_sub_category ( '7561' )

        payee = doc.find_payee ( name )
        if not payee:
            payee = doc.create_payee ( name )
            new = ' <span style="padding:0px 0.5em;background:#f00;color:#fff;">CREATED</span>'
        else:
            new = ''
        print "<tr><td><b>%s%s</b></td>" % ( name, new )

        found = 0
        transactions = payee.get_transactions ( )
        if transactions:
            for transaction in transactions:
                account = transaction.get_account ( )
                note = transaction . get_comment ( ) 
                if e.reference:
                    if account . get_name ( ) == u'TPE' and note and e.reference and note . find ( e.reference ) != -1:
                        found += 1
#                        print u"Found payment for %s (%s)" % ( e.reference, transaction . get_comment ( ) )
#                else:
#                    print u"No reference for %s" % e

        if not found:
#            print "Found no TPE transaction for %s, creating" % name
            print "<td>%s</td><td>%s</td><td>%.02f&euro;</td></tr>" % ( s.get_name(), e.payment_received_date.strftime('%d/%m/%Y'), e.amount )
            t = tpe . create_transaction ( payee )
            t.set_payment_mode ( 30 )
            t.set_category ( c, s )
            t.set_amount ( e.amount )
            t.set_date ( e.payment_received_date )
            t.set_financial_year_by_date ( e.payment_received_date )
            register ( e.amount, e.payment_received_date, True )
            if e.reference:
                t.set_comment ( u"Adhésion personne physique (saisie auto, commande %s" % e.reference )
            else:
                t.set_comment ( u"Adhésion personne physique (saisie auto)" )
#        else:
#            print "Found %d TPE transaction(s) for %s" % ( found, name )

    print "</table>"

    # SQLalchemy cleanup
    clear_mappers()
