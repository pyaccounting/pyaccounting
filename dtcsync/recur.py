# -*- coding: utf-8 -*-

import sys, re

sys.path.append('../../pydtc/pyDTC')
from pyDTC import *

sys.path.append('../pyaccounting')
from GrisbiDocument import *

from registration import register

def import_recur ( doc ):
    dtc=pyDTC('mysql://','pydtc.conf')
    cm = doc.find_account ( u'Crédit Mutuel' )
    tpe_payee = doc.find_payee ( u'TPE récurrent' ) 
    cion_payee = doc.find_payee ( u'TPE CION récurrent' ) 
    ( c, s ) = doc.find_category_sub_category ( '7560' )

    start_date = '-'.join ( re.match ( 'Sync auto (\d\d)/(\d\d)/(\d\d\d\d)', cm.get_comment ( ) ).group(3,2,1) )
    print """<h2>Importing recurrent CB from %s</h2>\n
    <table class="supertable">
    <tr><th>Date</th><th>Amount</th><th>Commission</th></tr>
    """ % start_date

    for e in dtc.query ( func.date(Subscription.payment_received_date).label('date') ).\
        add_column(func.sum(Subscription.cbrecur_prorata).label('prorata_sum')).\
        add_column(func.sum(func.round(Subscription.cbrecur_prorata*.009 + 0.1, 2)).label('prorata_cion')).\
        filter ( Subscription.payment_received_date != None ).\
        filter ( Subscription.cbrecur_prorata != None ).\
        filter ( Subscription.payment_received_date >= start_date ).\
        filter ( Subscription.payment_received_date < func.date(func.now()) ).\
        filter ( Subscription.payment_mode == 'cbweb_recur' ).\
        group_by ( func.date(Subscription.payment_received_date) ):

        found = doc.find_transactions ( { 'date': e.date,
                                          'account': cm,
                                          'payee': tpe_payee } )
        if not found:
            print "<td>%s</td><td>%.02f&euro;</td><td>-%.02f&euro;</td></tr>" % ( e.date.strftime('%d/%m/%Y'), e.prorata_sum, e.prorata_cion )
            t = cm . create_transaction ( tpe_payee )
            t.set_payment_mode ( 19 )
            t.set_category ( c, s )
            t.set_amount ( e.prorata_sum )
            t.set_date ( e.date )
            t.set_financial_year_by_date ( e.date )
            t.set_comment ( u"Prorata CB récurrente (saisie auto)" )

            t = cm . create_transaction ( cion_payee )
            t.set_payment_mode ( 19 )
            t.set_category ( c, s )
            t.set_amount ( - e.prorata_cion )
            t.set_date ( e.date )
            t.set_financial_year_by_date ( e.date )
            t.set_comment ( u"Comission prorata CB récurrente (saisie auto)" )

    print "</table>"

    # SQLalchemy cleanup
    clear_mappers()
