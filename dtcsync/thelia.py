# -*- coding: utf-8 -*-

import MySQLdb, sys, re, datetime, ConfigParser

sys.path.append('../pyaccounting')
from GrisbiDocument import *

from registration import *

def import_thelia ( doc ):
    config = ConfigParser.RawConfigParser()
    config_filename = 'pydtc.cfg'
    if not config.read(config_filename):
        raise Exception('Unable to read config file pydtc.cfg')
    
    cm = doc.find_account ( u'Crédit Mutuel' )
    tpe = doc.find_account ( u'TPE immédiat' )
    start_date = '-'.join ( re.match ( 'Sync auto (\d\d)/(\d\d)/(\d\d\d\d)', cm.get_comment ( ) ).group(3,2,1) )
    print """<h2>Importing Thelia from %s</h2>\n
    <table class="supertable">
    <tr><th>Day</th><th>ID</th><th>References</th><th>Date</th><th>Amount</th></tr>
    """ % start_date

    db = MySQLdb.connect(user=config.get('Thelia', 'User'), 
                        passwd=config.get('Thelia', 'Password'), 
                        host=config.get('Thelia', 'Host'), 
                        db=config.get('Thelia', 'DB'))

    cursor = db.cursor ( )
    cursor.execute("""
   SELECT CONCAT(client.prenom, ' ', client.nom),
          GROUP_CONCAT(TRIM(TRIM(BOTH '\n' FROM venteprod.titre))),
          GROUP_CONCAT(venteprod.ref),
          commande.id,
          commande.statut,
          DATE(commande.date),
          SUM((quantite * prixu)) + port
     FROM venteprod
     JOIN commande ON commande.id = venteprod.commande
     JOIN client ON client.id = commande.client
    WHERE statut >= 2 AND commande.date >= '%s' AND commande.date < DATE(NOW())
    GROUP BY commande.id;""" % start_date);

    while True:
        try:
            ( client, titre, ref, id, statut, date, total ) = cursor.fetchone ( )
        except TypeError:
            break
        print u"<tr><td><b>%s</b></td><td>%06d</td><td>%s</td><td>%s</td><td>%.02f&euro;</td></tr>" % ( unicode(client,'iso-8859-1'), id, ref, date.strftime('%d/%m/%Y'), float(total) )
        
        payee = doc.find_payee ( unicode(client, 'iso-8859-1') ) or doc.create_payee ( unicode(client, 'iso-8859-1') )
        t = tpe . create_transaction ( payee )
        if ref[:3] == 'DON':
            ( c, s ) = doc.find_category_sub_category ( '7588' )
        else:
            ( c, s ) = doc.find_category_sub_category ( '7070' )
        t.set_payment_mode ( 30 )
        t.set_category ( c, s )
        t.set_amount ( total )
        t.set_date ( date )
        t.set_financial_year_by_date ( date )
        t.set_comment ( u"Commande boutique (saisie auto, commande %06d: %s)" % ( id, unicode(ref, 'iso-8859-15') ) )
        register ( total, date, True )

    print "</table>"
