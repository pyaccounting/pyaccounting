============
pyAccounting
============

pyAccounting is a set of classes to handle accounting files, most
notably Grisbi files. This would ultimately allow bulk import from CMS
like DTC into accounting software.


Dependencies
============

* Python

* python-lxml

* python-datetime

* python-unicodedata


Homepage
========

See https://gitorious.org/pyaccounting

