#!/usr/bin/python
# -*- coding: utf-8 -*-

import sys, re, io, unicodedata, datetime
from lxml import etree

from XMLObject import *
from AccountingDocument import *

from GrisbiTransaction import *
from GrisbiScheduled import *
from GrisbiAccount import *
from GrisbiPayee import *
from GrisbiSubCategory import *
from GrisbiCategory import *


#------------------------------------------------------------------------------
class GrisbiDocument ( AccountingDocument, XMLObject ) :
    """..."""

    #----------------------------------------------------------------------
    def __init__ ( self, filename ):
        AccountingDocument.__init__ ( self, filename )
        self.parser = etree.XMLParser ( encoding = 'utf-8' )
        self.doc = etree.parse ( filename, self.parser )
        self.node = self.doc.getroot()
        self._last_transaction = None
        self._last_payee = None
        self._caches = { 'Party': { 'clean': False, 'keys': { } } }

    #----------------------------------------------------------------------
    def __str__ ( self ) :
        return '<GrisbiDocument %s>' % id(self)


    #----------------------------------------------------------------------
    def serialize_string ( self ) :
        return etree.tostring ( self.node, encoding="utf-8", method='html', \
                                with_tail=False, pretty_print=True )

    #----------------------------------------------------------------------
    def serialize ( self, filename ) :
        multiline_nodes = ( 'Account', 'Amount_comparison', 'Color', 'General', 'Print', 'Report', 'Text_comparison' )
        try:
            out = io.open ( filename, 'w', encoding='utf-8' )
            out.write ( u"<?xml version=\"1.0\"?>\n<Grisbi>\n" )
            for el in sorted ( self.node.iterdescendants(), GrisbiDocument.gsb_node_sort ):
                line = "\t<%s" % el.tag
                for attr in el.items():
                    val = attr[1].replace("&", "&amp;").\
                          replace("'", "&apos;").\
                          replace("\"", "&quot;").\
                          replace("<", "&lt;").\
                          replace(u"", "&#x80;").\
                          replace(">", "&gt;")
                    if el.tag in multiline_nodes:
                        line += "\n\t\t%s=\"%s\"" % (attr[0], val)
                    else:
                        line += " %s=\"%s\"" % (attr[0], val)
                if el.tag != 'Bet':
                    line += ' '
                line += "/>\n"
                out.write ( unicode(line) )
            out.write ( u"</Grisbi>" )
            out.close ( )
        except:
            print "Unexpected error:", sys.exc_info ( )
            raise
        
    @classmethod
    def gsb_node_sort (classname, node1, node2 ):
        nodes = { 'General': 0,
                  'Color': 1,
                  'Print': 2,
                  'Currency': 3,
                  'Account': 4,
                  'Payment': 5,
                  'Transaction': 6,
                  'Scheduled': 7,
                  'Party': 8,
                  'Category': 9,
                  'Sub_category': 9,
                  'Budgetary': 10,
                  'Sub_budgetary': 10,
                  'Bank': 12,
                  'Financial_year': 13,
                  'Archive': 14,
                  'Reconcile': 15,
                  'Bet': 16,
                  'Bet_graph': 17,
                  'Report': 18,         # Do not touch anything in reports (lame, I know)
                  'Amount_comparison': 18, # Do not touch anything in reports (lame, I know)
                  'Text_comparison': 18 } # Do not touch anything in reports (lame, I know)
        if nodes [ node1.tag ] == 9 and nodes [ node2.tag ] == 9 :
            if node1.tag == 'Category':
                c1 = node1.get('Nb')
            else:
                c1 = node1.get('Nbc')
            if node2.tag == 'Category':
                c2 = node2.get('Nb')
            else:
                c2 = node2.get('Nbc')
            if c1 != c2:
                return cmp ( int(c1), int(c2) )
            return cmp ( node2.tag == 'Category', node1.tag == 'Category' )
        if nodes [ node1.tag ] == 9 and nodes [ node2.tag ] == 9 :
            if node1.tag == 'Budgetary':
                c1 = node1.get('Nb')
            else:
                c1 = node1.get('Nbb')
            if node2.tag == 'Budgetary':
                c2 = node2.get('Nb')
            else:
                c2 = node2.get('Nbb')
            if c1 != c2:
                return cmp ( int(c1), int(c2) )
            return cmp ( node2.tag == 'Budgetary', node1.tag == 'Budgetary' )
        return cmp ( nodes [ node1.tag ], nodes [ node2.tag ] )

    #----------------------------------------------------------------------
    def find_account ( self, account ) :
        string = "/Grisbi/Account"
        nodes = self._xpath(string)
        unaccentued = self.unac_string ( account )
        if nodes is not None:
            for child in nodes:
                if self.string_equal ( self.unac_string ( unicode(child.get('Name')) ), unaccentued ):
                    return GrisbiAccount ( self, child )

    #----------------------------------------------------------------------
    def find_account_by_id ( self, id ) :
        string = "/Grisbi/Account[@Number='%d']" % int(id)
        return GrisbiAccount ( self, self._xpath_first(string) )

    #----------------------------------------------------------------------
    def get_last_transaction ( self ):
        if not self._last_transaction:
            maximum = 0
            for t in self._xpath ( '/Grisbi/Transaction' ):
                if t.get('Nb') and int(t.get('Nb')) > int(maximum):
                    maximum = int(t.get('Nb'))
            self._last_transaction = maximum
        return self._last_transaction

    #----------------------------------------------------------------------
    def increment_last_transaction ( self ):
        last_transaction = self.get_last_transaction ( )
        self._last_transaction += 1
        return self._last_transaction

    #----------------------------------------------------------------------
    def get_payee_last_id ( self ):
        if not self._last_payee:
            maximum = 0
            for t in self._xpath ( '/Grisbi/Party' ):
                if t.get('Nb') and int(t.get('Nb')) > int(maximum):
                    maximum = int(t.get('Nb'))
            self._last_payee = maximum
        return self._last_payee

    #----------------------------------------------------------------------
    def increment_last_payee_id ( self ):
        last_payee = self.get_payee_last_id ( )
        self._last_payee += 1
        return self._last_payee

    #----------------------------------------------------------------------
    def find_payee ( self, payee ) :
        if self._cache_unclean ( 'Party' ):
            self._rebuild_payee_cache ( )
        payee_name = self.unac_string ( payee )
        matched = self._cache_lookup ( 'Party', payee_name )
        if matched is not None:
            return GrisbiPayee ( self, matched )

    #----------------------------------------------------------------------
    def _rebuild_payee_cache ( self ):
        for node in self.node.iter ( 'Party' ):
            if ( node.get ( 'Na' ) ):
                node_payee_name = self.unac_string ( node . get ( 'Na' ) )
                self . _cache_insert ( 'Party', node_payee_name, node )
        self . _caches [ 'Party' ] [ 'clean' ] = True

    #----------------------------------------------------------------------
    def find_payee_by_id ( self, id ) :
        p = self._xpath_first ( "/Grisbi/Party[@Nb='%d']" % int ( id ) )
        if p is not None:
            return GrisbiPayee ( self, p )

    #----------------------------------------------------------------------
    def get_payees ( self ) :
        array = []
        for child in self.node.iter ( 'Party' ):
            array . append ( GrisbiPayee ( self, child ) )
        return array

    #----------------------------------------------------------------------
    def create_payee ( self, name ) :
        node = self.create_node ( None, 'Party' )
        payee = GrisbiPayee ( self, node, name )
        id = self.increment_last_payee_id ( )
        payee . set_id ( id )
        self._cache_insert ( 'Party', self.unac_string ( name ), node )
        return payee

    #----------------------------------------------------------------------
    def get_transactions_from_payee ( self, payee, predicates = False ):
        string = "/Grisbi/Transaction[@Pa='%s']" % payee.get_id ( )
        nodes = self.doc.getroot().xpath(string)
        result = [ ]
        if nodes:
            for node in nodes:
                if ( predicates ):
                    if ( predicates [ 'date' ] ):
                        if isinstance ( predicates [ 'date' ], datetime ):
                            d = predicates [ 'date' ]
                        else:
                            d = datetime.strptime(predicates [ 'date' ], '%Y-%m-%d')
                        if ( d != self._parse_french_date ( node.get('Da') ) ):
                            print "Skipping because %s != %s" % ( d, self._parse_french_date ( node.get('D') ) )
                            continue
                result.append ( GrisbiTransaction ( self, node ) )
            return result

    #----------------------------------------------------------------------
    def get_transaction ( self, predicates = False ):
        transactions = self . find_transactions ( predicates )
        if not transactions:
            return None
        if len ( transactions ) != 1:
            raise LookupError ( 'Lookup should return only 1 transaction (%d returned)' % len ( transactions ) )
        return transactions [ 0 ]

    #----------------------------------------------------------------------
    def find_transactions ( self, predicates = False ):
        predicate_list = []
        if ( predicates ):
            if ( 'date' in predicates ):
                if isinstance ( predicates [ 'date' ], datetime ):
                    predicate_list . append ( "@Dt='%s'" % predicates [ 'date' ] . strftime ( '%m/%d/%Y' ) )
                elif isinstance ( predicates [ 'date' ], date ):
                    predicate_list . append ( "@Dt='%s'" % predicates [ 'date' ] . strftime ( '%m/%d/%Y' ) )
                else:
                    predicate_list . append ( "@Dt='%s'" % datetime.strptime(predicates [ 'date' ], '%Y-%m-%d') . strftime ( '%m/%d/%Y' ) )
            if ( 'payee' in predicates ):
                    predicate_list . append ( "@Pa='%s'" % predicates [ 'payee' ] . get_id ( ) )
            if ( 'payment_mode' in predicates ):
                    predicate_list . append ( "@Pn='%s'" % predicates [ 'payment_mode' ] )
            if ( 'account' in predicates ):
                    predicate_list . append ( "@Ac='%s'" % predicates [ 'account' ] . get_id ( ) )
            if ( 'number' in predicates ):
                    predicate_list . append ( "@Nb='%s'" % predicates [ 'number' ] )
            if ( 'category' in predicates ):
                    predicate_list . append ( "@Ca='%s'" % predicates [ 'category' ] . get_id ( ) )
            if ( 'sub_category' in predicates ):
                    predicate_list . append ( "@Sca='%s'" % predicates [ 'sub_category' ] . get_id ( ) )

            string = "/Grisbi/Transaction[%s]" % ' and ' . join ( predicate_list )
        else:
            string = "/Grisbi/Transaction"

        nodes = self.doc.getroot().xpath(string)
        result = [ ]
        if nodes:
            for node in nodes:
                result.append ( GrisbiTransaction ( self, node ) )
            return result

    #----------------------------------------------------------------------
    def get_scheduled_from_payee ( self, payee ):
        string = "/Grisbi/Scheduled[@Pa='%s']" % payee
        nodes = self.doc.getroot().xpath(string)
        result = [ ]
        if nodes:
            for node in nodes:
                result.append ( GrisbiScheduled ( self, node ) )
            return result

    #----------------------------------------------------------------------
    def merge_payees ( self, payee1, payee2 ):
        """Merge two payees, only conserving the first.
        TODO: Merge payees in reports and recurrent transactions as well."""

        transactions = payee2.get_transactions ( )
        if transactions:
            for transaction in transactions:
                transaction.set_payee ( payee1 )

        scheduled = payee2.get_scheduled ( )
        if scheduled:
            for s in scheduled:
                s.set_payee ( payee1 )
        payee2.remove ( )

    #----------------------------------------------------------------------
    def remove_payee ( self, payee ):
        if payee.get_transactions ( ) is not None:
            raise RuntimeError ( 'Payee still has transactions, aborting.' )
        self._cache_remove ( 'Party', self.unac_string ( payee.get_name() ) )
        self.node.remove ( payee.node )

    #----------------------------------------------------------------------
    def find_category ( self, description ):
        nodes = self._xpath("/Grisbi/Category")
        if nodes is not None:
            for child in nodes:
                if self.string_match ( child.get('Na'), description ):
                    return GrisbiCategory ( self, child )

    #----------------------------------------------------------------------
    def find_category_by_id ( self, id ):
        node = self._xpath_first("/Grisbi/Category[@Nb=%s]" % id)
        if node is not None:
            return GrisbiCategory ( self, node )

    #----------------------------------------------------------------------
    def find_category_sub_category ( self, description ):
        nodes = self._xpath( "//Sub_category" )
        if nodes is not None:
            for child in nodes:
                if self.string_match ( child.get('Na'), description ):
                    sub_category = GrisbiSubCategory ( self, child )
                    if sub_category:
                        return ( sub_category . get_parent ( ), sub_category )
        return ( None, None )


    #----------------------------------------------------------------------
    def get_financial_year_by_date ( self, d ):
        nodes = self._xpath("/Grisbi/Financial_year")
        if isinstance ( d, str ):
            date = self._parse_french_date ( d )
        else:
            date = datetime(d.year,d.month,d.day)
        if nodes is not None:
            for child in nodes:
                start = self._parse_french_date ( child . get ( 'Bdte' ) )
                end = self._parse_french_date ( child . get ( 'Edte' ) )
                if start <= date <= end:
                    return child.get ( 'Nb' )
        raise RuntimeError ( 'Unable to find financial year for date %s' % d )
        
    #----------------------------------------------------------------------
    def get_type ( self, type ):
        node = self._xpath_first ( '/Grisbi/Payment[@Number=%s]' % type )
        if node is not None:
            return node.get ( 'Name' )
