# -*- coding: utf-8 -*-
# PyAccounting
# Copyright (C) 2011 Benjamin Drieu
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see
# <http://www.gnu.org/licenses/>.


from Scheduled import *
from XMLObject import *
from lxml import etree
from datetime import *


#------------------------------------------------------------------------------
class GrisbiScheduled ( Scheduled, XMLObject ) :
    """Class to handle a Grisbi scheduled transaction."""

    #----------------------------------------------------------------------
    def __init__ ( self, document, node, init = False ):
        XMLObject.__init__ ( self, document, node )
        Scheduled.__init__ ( self )
        self.document = document
        self.node = node
        if init:                # Set all defaults
            self.node_set ( 'Date', '0/0/0' )
            self.node_set ( 'Montant', '0,0000000' )
            self.node_set ( 'Virement_compte', '0' )
            self.node_set ( 'Type', '1' )
            self.node_set ( 'Type_conte_ope', '0' )
            self.node_set ( 'Contenu_du_type', '' )
            self.node_set ( 'Imputation', '0' )
            self.node_set ( 'Sous-imputation', '0' )
            self.node_set ( 'Notes', '' )
            self.node_set ( 'Automatique', '1' )
            self.node_set ( 'Periodicite', '4' )
            self.node_set ( 'Intervalle_periodicite', '1' )
            self.node_set ( 'Date_limite', '' )
            self.node_set ( 'Ech_ventilee', '0' )
            self.node_set ( 'No_ech_associee', '0' )
            self.set_date ( date.today ( ) )


    #----------------------------------------------------------------------
    def __str__ ( self ) :
        return '<GrisbiScheduled %s %6d: \'%s\'>' % ( self . get_payment_mode ( ), 
                                                        int ( self . get_id ( ) ), 
                                                        self . get_comment ( ) )

    #----------------------------------------------------------------------
    def get_id ( self ):
        if self.node is not None:
            return self.node_get('No').encode('utf-8')

    #----------------------------------------------------------------------
    def set_id ( self, id ):
        if self.node is not None:
            self.node_set ( 'No', id )

    #----------------------------------------------------------------------
    def get_account ( self ):
        if self.node is not None:
            print "Account for %s is %s" % ( self.node_get('No'), self.document.find_account_by_id ( int ( self.node_get ( 'Compte' ) ) ) )
            return self.document.find_account_by_id ( int ( self.node_get ( 'Compte' ) ) )

    #----------------------------------------------------------------------
    def get_amount ( self ):
        if self.node is not None and self.node_get ( 'Montant' ) is not None:
            return self.node_get('Montant').encode('utf-8')
        else:
            return None

    #----------------------------------------------------------------------
    def set_amount ( self, amount ):
        if self.node is not None:
            self.node_set ( 'Montant', "%.02f" % float ( amount ) )
            account = self.get_account ( )

    #----------------------------------------------------------------------
    def get_comment ( self ):
        if self.node is not None and self.node_get ( 'Notes' ) is not None:
            return self.node_get('Notes').encode('utf-8')
        else:
            return None

    #----------------------------------------------------------------------
    def set_comment ( self, comment ):
        if self.node is not None:
            self.node_set ( 'Notes', comment )

    #----------------------------------------------------------------------
    def get_date ( self ):
        if self.node is not None and self.node_get ( 'Date' ) is not None:
            return datetime.strptime ( self.node_get('Date').encode('utf-8'), 
                                       '%d/%m/%Y')
        else:
            return None

    #----------------------------------------------------------------------
    def set_date ( self, d ):
        if self.node is not None:
            self.node_set ( 'Date', '%02d/%02d/%04d' % ( d.day, d.month, d.year ) )

    #----------------------------------------------------------------------
    def get_financial_year ( self ):
        if self.node is not None and self.node_get ( 'Exercice' ) is not None:
            return financial_yeartime.strptime ( self.node_get('Exercice').encode('utf-8'), 
                                                 '%d/%m/%Y')
        else:
            return None

    #----------------------------------------------------------------------
    ## Set financial year from a date string representation.
    # \resume The financial year Id is obtained from a document lookup
    #
    # \param self
    # \param d          French string representation of a date.  i.e. 1/1/2012 
    #                   or datetime.date object
    def set_financial_year ( self, d ):
        if self.node is not None:
            self.node_set ( 'Exercice', self.document.get_financial_year_by_date ( d ) )

    #----------------------------------------------------------------------
    def get_payment_mode ( self ):
        if self.node is not None:
            account = self . get_account ( )
            mode = account . get_type ( self.node_get ( 'Type' ) )
            if mode:
                return mode.encode ( 'utf-8' )

    #----------------------------------------------------------------------
    def get_category ( self ):
        return self.document.find_category_by_id ( self.node_get ( 'Categorie' ) )

    #----------------------------------------------------------------------
    def set_category ( self, category, sub_category = None ):
        if self.node is not None:
            self.node_set ( 'Categorie', category.get_id ( ) )
            if sub_category is not None:
                self.node_set ( 'Sous-categorie', sub_category.get_id ( ) )
        
    #----------------------------------------------------------------------
    def get_payee ( self ):
        if self.node is not None:
            return self.document.find_payee_by_id ( self.node_get ( 'Tiers' ) )

    #----------------------------------------------------------------------
    def set_payee ( self, payee ):
        if self.node is not None:
            self.node_set ( 'Tiers', payee . get_id ( ) )
        

from GrisbiAccount import *
