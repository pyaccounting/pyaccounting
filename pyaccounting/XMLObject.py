import os
from lxml import etree
from datetime import *
from unac import unac

class XMLObject(object):

    def __init__ ( self, document, node ):
        None
#        if os . environ . has_key ( 'DEBUG' ):
#            print "Mapped: %s -> %s" % ( node.getparent(), node )

    def _set(self,attr,val):
        self.node.set ( attr, unicode(val) )

    def _get ( self, attr ):
        return self.node.get ( attr )

    def __getattr__ ( self, attr ):
        if attr[:4] == 'set_' and self.automatic_attributes.has_key(attr[4:]):
            def wrapper(*args, **kw):
                return self._set(self.automatic_attributes[attr[4:]],args[0])
            return wrapper
        if attr[:4] == 'get_' and self.automatic_attributes.has_key(attr[4:]):
            def wrapper(*args, **kw):
                return self._get(self.automatic_attributes[attr[4:]])
            return wrapper
        raise AttributeError(attr)

    #----------------------------------------------------------------------
    def _xpath ( self, string ) :
        if os . environ . has_key ( 'DEBUG' ):
            print "XPATH: %s %s" % ( self.node, string )
        return self.node.xpath ( string )

    #----------------------------------------------------------------------
    def _xpath_first ( self, string ) :
        list = self._xpath ( string )
        if len ( list ) > 0:
            return list [ 0 ]

    #----------------------------------------------------------------------
    def _parse_french_date ( self, d ):
        try:
            return datetime.strptime ( d, '%m/%d/%Y' )
        except Exception as e:
            try:
                return datetime.strptime ( d, '%Y-%m-%d' )
            except Exception as e:
                print e, d
        
    #----------------------------------------------------------------------
    def node_set ( self, var, val ):
        if self.node is not None:
            if isinstance ( val, int ):
                val = "%d" % val
            self.node.set ( var, val )
        if os . environ . has_key ( 'DEBUG' ):
            print "Setting %s : %s = %s" % ( self, var, val )

    #----------------------------------------------------------------------
    def node_get ( self, var ):
        if self.node is not None:
            if os . environ . has_key ( 'DEBUG' ):
                print "Getting : %s == %s" % ( var, self.node.get ( var ) )
            return self.node.get ( var )

    #----------------------------------------------------------------------
    def node_getparent ( self ):
        if self.node is not None:
            return self.node.getparent ( )

    #----------------------------------------------------------------------
    def create_node ( self, parent, name ):
        if parent is None:
            parent = self.node
        return etree.SubElement ( parent, name )

    #----------------------------------------------------------------------
    def string_equal ( self, str1, str2 ):
        if str1 is None or str2 is None:
            result = str1 is None and str2 is None
        else:
            result = str1 . lower ( ) == str2 . lower ( )
#        if os . environ . has_key ( 'DEBUG' ):
#            print "%s == %s ? %d" % ( str1, str2, result )
        return result

    #----------------------------------------------------------------------
    def string_match ( self, haystack, needle ):
        if haystack is None or needle is None:
            result = haystack is None and needle is None
        else:
            result = ( haystack . lower ( ) . find ( needle . lower ( ) ) != -1 )
#        if os . environ . has_key ( 'DEBUG' ):
#            print "%s matches %s ? %d" % ( haystack, needle, result )
        return result

    #----------------------------------------------------------------------
    def ucwords ( self, s ):
        """Returns a string with the first character of each word in str 
        capitalized, if that character is alphabetic."""
        if s.upper ( ) == s:
            return s

        s = s . capitalize ( )
        for sep in (' ','\t','\v','\n','\r','\f', '-', '\''):
            a = s.split(sep)
            r = []
            for x in a:
                r.append ( x[0:1] . capitalize ( ) + x[1:] )
            s = sep . join ( r )
        return s

    #----------------------------------------------------------------------
    def unac_string ( self, s ):
        """Return a unaccentuated string."""
        if type (s) == type (""):
            s = unicode (s)
	try:
	    out = unac.unac_string ( s, 'utf-8' )
	except UnicodeDecodeError as e:
	    try:
		string = s . decode ( 'utf-8' )
		ustring = string . encode ( 'raw_unicode_escape' ) . decode ( 'utf-8' )
		out = unac.unac_string ( ustring, 'UTF-8' )
	    except UnicodeDecodeError as e:
                out = s
        return out.replace ( '-', ' ' ) . lower ( )


    #----------------------------------------------------------------------
    def _cache_unclean ( self, cache ):
        if cache in self._caches and self._caches [ cache ] [ 'clean' ]:
            return False
        else:
            return True

    #----------------------------------------------------------------------
    def _cache_lookup ( self, cache, key ):
        if os . environ . has_key ( 'DEBUG' ):
            print "Looking up key %s/%s" % ( cache, key )
        if self._caches and key in self._caches [ cache ] [ 'keys' ]:
            if os . environ . has_key ( 'DEBUG' ):
                print "Found %s" % self._caches [ cache ] [ 'keys' ] [ key ]
            return self._caches [ cache ] [ 'keys' ] [ key ]

    #----------------------------------------------------------------------
    def _cache_insert ( self, cache, key, value ):
        if os . environ . has_key ( 'DEBUG' ):
            print "Inserting key %s(%s)" % ( key, value )
        self._caches [ cache ] [ 'keys' ] [ key ] = value

    #----------------------------------------------------------------------
    def _cache_remove ( self, cache, key ):
        self._caches [ cache ] [ 'keys' ] [ key ]



