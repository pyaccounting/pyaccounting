# -*- coding: utf-8 -*-
# PyAccounting
# Copyright (C) 2011 Benjamin Drieu
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see
# <http://www.gnu.org/licenses/>.

from lxml import etree
from Account import *
from XMLObject import *
from decimal import *

from GrisbiTransaction import *


#------------------------------------------------------------------------------
class GrisbiAccount ( Account, XMLObject ) :
    """Class to handle a Grisbi account."""

    automatic_attributes = { 'comment': 'Comment' }

    #----------------------------------------------------------------------
    def __init__ ( self, document, node ):
        XMLObject.__init__ ( self, document, node )
        Account.__init__ ( self )
        self.node = node
        self.document = document

    #----------------------------------------------------------------------
    def __str__ ( self ) :
        return '<GrisbiAccount %s>' % self . get_name ( )

    #----------------------------------------------------------------------
    def get_id ( self ):
        if self.node is not None:
            return self.node.get('Number')

    #----------------------------------------------------------------------
    def get_name ( self ):
        if self.node is not None:
            return self.node.get('Name')
        else:
            return '(null)'

    #----------------------------------------------------------------------
    def create_transaction ( self, payee ):
        # FIXME: create it at the right place
        node = self.document.create_node ( self._xpath_first ( '/Grisbi' ), 'Transaction' )
        n = self.document.increment_last_transaction ( )
        transaction = GrisbiTransaction ( self.document, node, True, n )
        transaction . set_account ( self )
        transaction . set_payee ( payee )
        transaction . set_date ( date . today ( ) )
        return transaction

    #----------------------------------------------------------------------
    def create_transfert ( self, payee, account ):
        contra_account = self.document.find_account ( account )
        if not contra_account:
            raise RuntimeError ( "Unable to find account '%s'" % account )

        contra_transaction = contra_account.create_transaction ( payee )
        transaction = self.create_transaction ( payee )
        transaction.set_contra ( contra_account, contra_transaction )
        contra_transaction.set_contra ( self, transaction )

