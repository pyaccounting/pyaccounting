# -*- coding: utf-8 -*-
# PyAccounting
# Copyright (C) 2011 Benjamin Drieu
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see
# <http://www.gnu.org/licenses/>.

from lxml import etree
from Category import *
from XMLObject import *
from GrisbiSubCategory import *


#------------------------------------------------------------------------------
class GrisbiCategory ( Category, XMLObject ) :
    """Class to handle a Grisbi category."""

    automatic_attributes = { 'name': 'Na' }

    #----------------------------------------------------------------------
    def __init__ ( self, document, node, name = None ):
        XMLObject.__init__ ( self, document, node )
        Category.__init__ ( self )

        self.node = node
        self.document = document
        if ( name ):
            self.set_name ( name )

    #----------------------------------------------------------------------
    def __unicode__ ( self ) :
        return '<GrisbiCategory %s, %s>' % ( self.get_id ( ), self.get_name ( ) )

    #----------------------------------------------------------------------
    def get_id ( self ):
        if self.node is not None and self.node_get('Nb'):
            return self.node_get('Nb')

    #----------------------------------------------------------------------
    def set_id ( self, id ):
        if self.node is not None:
            return self.node_set('Nb', str ( id ) )

    #----------------------------------------------------------------------
    def get_sub_categories ( self ):
        if self.node is not None:
            result = [ ]
            children = self._xpath ( "/Grisbi/Sub_category[Nbc='%s']", self.get_id ( ) )
            for child in children:
                result.append ( GrisbiSubCategory ( self, child ) )
            return result
