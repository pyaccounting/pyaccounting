# -*- coding: utf-8 -*-
# PyAccounting
# Copyright (C) 2011 Benjamin Drieu
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see
# <http://www.gnu.org/licenses/>.


from Transaction import *
from XMLObject import *
from lxml import etree
from datetime import *
from decimal import *


#------------------------------------------------------------------------------
class GrisbiTransaction ( Transaction, XMLObject ) :
    """Class to handle a Grisbi transaction."""

    automatic_attributes = { 'payment_mode': 'Pn',
                             'financial_year': 'Fi',
                             'contra_transaction': 'Trt' }

    #----------------------------------------------------------------------
    def __init__ ( self, document, node, init = False, id = None ):
        XMLObject.__init__ ( self, document, node )
        Transaction.__init__ ( self )
        self.document = document
        self.node = node
        if init:                # Set all defaults
            self.node_set ( 'Ac', '(null)' )
            if id is not None:
                self.node_set ( 'Nb', id )
            self.node_set ( 'Id', '(null)' )
            self.node_set ( 'Dt', '0/0/0' )
            self.node_set ( 'Dv', '(null)' )
            self.node_set ( 'Cu', '1' ) # XXX Fix me not hardcoded
            self.node_set ( 'Am', '(null)' )
            self.node_set ( 'Exb', '0' )
            self.node_set ( 'Exr', '0.00' )
            self.node_set ( 'Exf', '0.00' )
            self.node_set ( 'Pa', '(null)' )
            self.node_set ( 'Ca', '0' )
            self.node_set ( 'Sca', '0' )
            self.node_set ( 'Br', '0' )
            self.node_set ( 'No', '' )
            self.node_set ( 'Pn', '0' )  # XXX Fix me not hardcoded
            self.node_set ( 'Pc', '(null)' )
            self.node_set ( 'Ma', '0' )
            self.node_set ( 'Ar', '0' )
            self.node_set ( 'Au', '0' )
            self.node_set ( 'Re', '0' )
            self.node_set ( 'Fi', '-2' )
            self.node_set ( 'Bu', '0' )
            self.node_set ( 'Sbu', '0' )
            self.node_set ( 'Vo', '(null)' )
            self.node_set ( 'Ba', '(null)' )
            self.node_set ( 'Trt', '0' )
            self.node_set ( 'Mo', '0' )
            self.set_date ( date.today ( ) )


    #----------------------------------------------------------------------
    def __str__ ( self ) :
        return '<GrisbiTransaction %s %6d: \'%s\'>' % ( self . get_payment_mode ( ), 
                                                        int ( self . get_id ( ) ), 
                                                        self . get_comment ( ) )
    
    #----------------------------------------------------------------------
    def get_id ( self ):
        if self.node is not None and self.node_get('Nb'):
            return self.node_get('Nb')
        else:
            return -1

    #----------------------------------------------------------------------
    def set_id ( self, id ):
        if self.node is not None:
            self.node_set ( 'Nb', id )

    #----------------------------------------------------------------------
    def get_account ( self ):
        if self.node is not None:
            return self.document.find_account_by_id ( self.node.get ( 'Ac' ) )

    #----------------------------------------------------------------------
    def set_account ( self, account ):
        if self.node is not None:
            self.node.set ( 'Ac', account.get_id ( ) )

    #----------------------------------------------------------------------
    def get_amount ( self ):
        if self.node is not None and self.node_get ( 'Am' ) is not None:
            return Decimal ( self.node_get('Am') )
        else:
            return None

    #----------------------------------------------------------------------
    def set_amount ( self, amount ):
        if self.node is not None:
            self.node_set ( 'Am',  "%.02f" % float ( amount ) )
# XXX TODO UPDATE ANY RELATED TRANSACTION

    #----------------------------------------------------------------------
    def get_comment ( self ):
        if self.node is not None and self.node_get ( 'No' ) is not None:
            return self.node_get('No')
        else:
            return None

    #----------------------------------------------------------------------
    def set_comment ( self, comment ):
        if self.node is not None:
            self.node_set ( 'No', comment )

    #----------------------------------------------------------------------
    def get_date ( self ):
        if self.node is not None and self.node_get ( 'Dt' ) is not None:
            return datetime.strptime ( self.node_get('Dt'), 
                                       '%m/%d/%Y').date()
        else:
            return None

    #----------------------------------------------------------------------
    def set_date ( self, d ):
        if isinstance(d, str):
            d = datetime.strptime ( d, '%Y-%m-%d').date()
        if self.node is not None:
            self.node_set ( 'Dt', '%02d/%02d/%04d' % ( d.month, d.day, d.year ) )

    #----------------------------------------------------------------------
    ## Set financial year from a date string representation.
    # \resume The financial year Id is obtained from a document lookup
    #
    # \param self
    # \param d          French string representation of a date.  i.e. 1/1/2012 
    #                   or datetime.date object
    def set_financial_year_by_date ( self, d ):
        if self.node is not None:
            self.node_set ( 'Fi', self.document.get_financial_year_by_date ( d ) )

    #----------------------------------------------------------------------
    def get_payment_mode ( self ):
        if self.node is not None:
            mode = self . document . get_type ( self.node_get ( 'Pn' ) )
            if mode:
                return mode

    #----------------------------------------------------------------------
    def get_category ( self ):
        return self.document.find_category_by_id ( self.node_get ( 'Ca' ) )

    #----------------------------------------------------------------------
    def set_category ( self, category, sub_category = None ):
        if self.node is not None:
            self.node_set ( 'Ca', category.get_id ( ) )
            if sub_category is not None:
                self.node_set ( 'Sca', sub_category.get_id ( ) )
        
    #----------------------------------------------------------------------
    def get_payee ( self ):
        if self.node is not None:
            return self.document.find_payee_by_id ( self.node_get ( 'Pa' ) )

    #----------------------------------------------------------------------
    def set_payee ( self, payee ):
        if self.node is not None:
            self.node_set ( 'Pa', payee . get_id ( ) )

    #----------------------------------------------------------------------
    def get_reference ( self ):
        if self.node is not None:
            return self.node_get('Ba')

    #----------------------------------------------------------------------
    def set_reference ( self, ref ):
        if self.node is not None and ref is not None:
            self.node_set ( 'Ba', ref )

    #----------------------------------------------------------------------
    def get_voucher ( self ):
        if self.node is not None:
            return self.node_get('Vo')

    #----------------------------------------------------------------------
    def set_voucher ( self, ref ):
        if self.node is not None and ref is not None:
            self.node_set ( 'Vo', ref )

    #----------------------------------------------------------------------
    def get_contra_transaction ( self ):
        return self.document.get_transaction ( { 'number' : self . node . get ( 'Trt' ) } )

    #----------------------------------------------------------------------
    def set_contra_transaction ( self, contra ):
        self.node.set ( 'Trt', contra . get_id ( ) )

    #----------------------------------------------------------------------
    def link_to_transaction ( self, transaction ):
        self.set_contra_transaction ( transaction )
        transaction.set_contra_transaction ( self )
        

from GrisbiAccount import *
