from distutils.core import setup

setup(
    name='pyaccounting',
    version='0.1.0',
    author='Benjamin Drieu',
    author_email='bdrieu@april.org',
    packages=['pyaccounting', 'pyaccounting.test'],
    scripts=['bin/dtcsync.py','bin/dtcsync-prelevements.py'],
    url='https://gitorious.org/pyaccounting',
    license='LICENSE.txt',
    description='Accounting files parsing and handling classes.',
    long_description=open('README.txt').read(),
    install_requires=[
        "lxml"
    ],
)
